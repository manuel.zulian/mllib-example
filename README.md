Just testing out spark mllib on the iris dataset.
The dataset is external, every label has been converted to a number between 0 and 2 and with
the default parameters a NaiveBayes gets 90-91% accuracy.