import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.classification.{NaiveBayes, NaiveBayesModel}
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint

/**
  * Created by manuel.zulian on 08/03/2017.
  */
object NaiveBayesExample {
  def main(args: Array[String]): Unit = {
    val sc = new SparkContext(new SparkConf().setAppName("NaiveBayes"))

    val data = sc.textFile(args(0))
    val parsedData = data.map { line =>
      val parts = line.split(',')
      LabeledPoint(parts(4).toDouble, Vectors.dense(parts(0).toDouble, parts(1).toDouble, parts(2).toDouble, parts(3).toDouble))
    }
    // Split data into training (60%) and test (40%).
    val splits = parsedData.randomSplit(Array(0.6, 0.4), seed = 11L)
    val training = splits(0)
    val test = splits(1)

    val model = NaiveBayes.train(training)

    val predictionAndLabel = test.map(p => (model.predict(p.features), p.label))
    val accuracy = 1.0 * predictionAndLabel.filter(x => x._1 == x._2).count() / test.count()

    println(accuracy)

    // Save and load model
    // model.save(sc, "myModelPath")
    // val sameModel = NaiveBayesModel.load(sc, "myModelPath")
  }
}
